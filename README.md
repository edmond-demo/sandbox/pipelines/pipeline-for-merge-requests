In a basic configuration, GitLab runs a pipeline each time changes are pushed to a branch.

If you want the pipeline to run jobs only when merge requests are created or updated, you can use pipelines for merge requests.

In the UI, these pipelines are labeled as detached. Otherwise, these pipelines appear the same as other pipelines.

Any user who has developer permissions can run a pipeline for merge requests.